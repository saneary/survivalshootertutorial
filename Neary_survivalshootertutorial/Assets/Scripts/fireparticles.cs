﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireparticles : MonoBehaviour
{
    EnemyHealth enemyHealth; //references enemy's health
    public float sliderValue = 1F; //affects height of flame
    public float sliderValue2 = 1F; //affects width of flame
    public float sliderValue3 = 0F; //value for no flame
    private ParticleSystem ps; //reference to particle system
    void Awake()
    {
        enemyHealth = GetComponentInParent<EnemyHealth>(); //gets component from enemy script
    }
    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>(); //gets component from particle system
    }

    // Update is called once per frame
    void Update()
    {
        
        if (enemyHealth.currentHealth <= 50) //if the enemy is less than half health...
        {
            //print("hi");
            // ... set the destination of the nav mesh agent to the player.
            ps.transform.localScale = new Vector3(sliderValue2, sliderValue, sliderValue); //then the flame changes
        }
        if (enemyHealth.currentHealth <= 0) //if the enemy is less than half health...
        {
            //print("hi");
            // ... set the destination of the nav mesh agent to the player.
            ps.transform.localScale = new Vector3(sliderValue3, sliderValue3, sliderValue3); //then the flame changes
        }
    }
}
